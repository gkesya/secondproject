using System.Collections.Generic;

public class UserWithTasksRequest
{
    public string Name { get; set; }
    public List<TaskRequest> Tasks { get; set; }
}

public class TaskRequest
{
    public string TaskDetail { get; set; }
}