﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using FS.Models;

namespace FS.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddUserWithTask(UserWithTasksRequest userWithTasksRequest)
        {
            // Kode untuk menyimpan user dan tasks ke database
            // Anda perlu menggunakan koneksi database dan operasi CRUD untuk menyimpan data ke tabel Users dan Tasks

            // Contoh kode untuk menyimpan data user dan tasks ke database
            using (var dbContext = new YourDbContext()) // Ganti YourDbContext dengan DbContext yang sesuai di aplikasi Anda
            {
                // Membuat objek User dari data yang diterima
                var user = new User
                {
                    Name = userWithTasksRequest.Name
                };

                // Menyimpan user ke tabel Users
                dbContext.Users.Add(user);
                dbContext.SaveChanges();

                // Membuat objek Task dari data yang diterima
                foreach (var taskDetail in userWithTasksRequest.Tasks)
                {
                    var task = new Task
                    {
                        TaskDetail = taskDetail.TaskDetail,
                        UserID = user.PkUsersID
                    };

                    // Menyimpan task ke tabel Tasks
                    dbContext.Tasks.Add(task);
                }

                dbContext.SaveChanges();
            }

            return Ok();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}