1. Membuat database beserta tabel dan komponen 

CREATE TABLE users (
    pk_users_id INT PRIMARY KEY,
    name VARCHAR(255)
);
CREATE TABLE tasks (
    pk_tasks_id INT PRIMARY KEY,
    task_detail VARCHAR(MAX),
    fk_users_id INT,
);

2. Membuat proyek pada vscode dan menjalankannya :

- dotnet -h
- dotnet new 
- dotnet new mvc
- dotnet run 

3. URL yang dijalankan pada postman 

"https://localhost:5001/api/tasks/GetUserWithTask